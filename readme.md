````aidl
https://www.callicoder.com/spring-boot-actuator/
````
```Hints``` </br>
APi  to fetch all metrices 
http://localhost:8080/actuator/metrics

To get the details of an individual metric, you need to pass the metric name in the URL like this -

http://localhost:8080/actuator/metrics/{MetricName}.

Eg. http://localhost:8080/actuator/metrics/system.cpu.usage
